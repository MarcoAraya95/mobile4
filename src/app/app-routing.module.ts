import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'forgot', loadChildren: './forgot/forgot.module#ForgotPageModule' },
  { path: 'alumno', loadChildren: './alumno/alumno.module#AlumnoPageModule' },
  { path: 'profesor', loadChildren: './profesor/profesor.module#ProfesorPageModule' },
  { path: 'listalumno', loadChildren: './listalumno/listalumno.module#ListalumnoPageModule' },
  { path: 'listprofesor', loadChildren: './listprofesor/listprofesor.module#ListprofesorPageModule' },
  { path: 'grafalumno', loadChildren: './grafalumno/grafalumno.module#GrafalumnoPageModule' },
  { path: 'avisoprofe', loadChildren: './avisoprofe/avisoprofe.module#AvisoprofePageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
