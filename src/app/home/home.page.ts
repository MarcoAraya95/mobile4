import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Service1Service } from '../services/service1.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loading: any;
  loginData = { rut:'', password:'' };
  data: any;

  constructor(public authService: Service1Service) {}

  doLogin() {
    this.authService.login(this.loginData).then((result) => {
      this.loading.dismiss();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
    }, (err) => {
      this.loading.dismiss();
    });
  }
  
}


  