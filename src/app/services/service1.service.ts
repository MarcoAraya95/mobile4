import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';

let apiUrl = "https://api.sebastian.cl/academia/swagger-ui.html#/";

@Injectable({
  providedIn: 'root'
})
export class Service1Service {

  constructor(public http: Http) { }

  login(credentials) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(apiUrl+'authentication-rest-impl/authenticateUsingPOST', JSON.stringify(credentials), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }

}

