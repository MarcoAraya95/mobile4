import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoprofePage } from './avisoprofe.page';

describe('AvisoprofePage', () => {
  let component: AvisoprofePage;
  let fixture: ComponentFixture<AvisoprofePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvisoprofePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoprofePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
