import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AvisoprofePage } from './avisoprofe.page';

const routes: Routes = [
  {
    path: '',
    component: AvisoprofePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AvisoprofePage]
})
export class AvisoprofePageModule {}
