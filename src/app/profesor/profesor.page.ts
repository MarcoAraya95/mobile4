import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Color } from '@ionic/core';
import { ɵangular_packages_forms_forms_f } from '@angular/forms';
import { Colors } from 'ng2-charts';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.page.html',
  styleUrls: ['./profesor.page.scss'],
})
export class ProfesorPage implements OnInit {

  user = "";

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.paramMap.get('id');

  }

  // pie
  num1 = 0.6;
  public pieChartLabels:string[] = ['Aprobados', 'Reprobados'];
  public pieChartData:Number[] = [80,20];
  public pieChartColor:Colors[] = [{
    backgroundColor: [
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 99, 132, 0.2)',
    ]}];

  public pieChartType:string = 'pie';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
}
