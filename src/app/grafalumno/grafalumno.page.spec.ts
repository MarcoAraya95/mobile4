import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrafalumnoPage } from './grafalumno.page';

describe('GrafalumnoPage', () => {
  let component: GrafalumnoPage;
  let fixture: ComponentFixture<GrafalumnoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrafalumnoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrafalumnoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
