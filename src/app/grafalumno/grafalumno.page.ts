import { Component, OnInit } from '@angular/core';
import { Colors } from 'ng2-charts';

@Component({
  selector: 'app-grafalumno',
  templateUrl: './grafalumno.page.html',
  styleUrls: ['./grafalumno.page.scss'],
})
export class GrafalumnoPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  // pie
  num1 = 0.6;
  public lineChartLabels:string[] = ['2013', '2014','2015','2016','2017','2018'];
  public lineChartData:any[] = [
    {data: [45,40,42,47,52,50], label: 'Yo'},
    {data: [50,45,47,45,47,48], label: 'Resto'}
  ];
  public lineChartType:string = 'line';


  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
}
