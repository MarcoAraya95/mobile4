import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HomePage } from './home/home.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  //public rootPage: any = HomePage;
  public appPages = [
    {
      title: 'Home Alumno',
      url: '/alumno',
      icon: 'home'
    },
    {
      title: 'Listado Alumno',
      url: '/listalumno'
    },
    {
      title: 'Grafico',
      url: '/grafalumno'
    },
    {
      title:'------------------'
    },
    {
      title: 'Home Profesor',
      url: '/profesor'
    },
    {
      title: 'Listado Profesor',
      url: '/listprofesor'
    },
    {
      title: 'Aviso Profesor',
      url: '/avisoprofe'
    },
    {
      title: 'Cerrar Sesion',
      url: '/home',
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
