import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PercentPipe } from '@angular/common';
import { Colors } from 'ng2-charts';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.page.html',
  styleUrls: ['./alumno.page.scss'],
})
export class AlumnoPage implements OnInit {
  
  user = "";

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.paramMap.get('id');

  }
  // pie
  num1 = 0.6;
public pieChartLabels:string[] = ['Aprobados', 'Reprobados'];
public pieChartData:Number[] = [60,40];
public pieChartColor:Colors[] = [{
  backgroundColor: [
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 99, 132, 0.2)',
  ]}];
public pieChartType:string = 'pie';


// events
public chartClicked(e:any):void {
  console.log(e);
}

public chartHovered(e:any):void {
  console.log(e);
}
}
