import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListalumnoPage } from './listalumno.page';

describe('ListalumnoPage', () => {
  let component: ListalumnoPage;
  let fixture: ComponentFixture<ListalumnoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListalumnoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListalumnoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
