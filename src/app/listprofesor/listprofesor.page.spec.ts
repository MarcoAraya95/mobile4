import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListprofesorPage } from './listprofesor.page';

describe('ListprofesorPage', () => {
  let component: ListprofesorPage;
  let fixture: ComponentFixture<ListprofesorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListprofesorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListprofesorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
